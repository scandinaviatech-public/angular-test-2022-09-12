# Front-end Technical Test
 
This is a technical test built by the Scandinaviatech team.  
The purpose of this test is to find the experience level of the applicants.
 
The test covers basic interaction with the google maps API and WebSockets.
 
# Application Description
 
A company has several GPS tracking devices attached to their vehicles.  
Each device periodically sends location information about the vehicle location in the form of longitude and latitude pairs such as:
 
```json
{
  lng: <number>;
  lat: <number>;
}
```
 
Each vehicle has a unique identifier `id`.
 
You can listen to the device location events by connecting to the server using WebSockets Protocol, sending a `join` event with the device id and listening to the `location` event.
 
## Page layout
 
- A list of company vehicles.
- A map (google maps preferably)
 
## Requirements
 
- The user can check or uncheck a vehicle from the list
- When the vehicle is checked you should send a `join` event with the vehicle `id` property
- Once you join a vehicle room you will start receiving location events.  
<small> Note: since this is a simulation and not real data , once the events stop you will need to leave the room and re-join to start receiving events once again </small>.
- When the vehicle is unchecked you should send a `leave` event with the vehicle `id` property.
- Once you receive the first location you should place a marker on the map representing that vehicle.
- When further events are received you should update the marker location on the map.
 
## Bonus Points
- While the marker is being updated, draw a line showing the vehicle driving path, ( behind the marker ).
-   Allow the user to have full control of the map(move around and zoom in the map) while the markers are being updated.
 
# Rules and Guidelines
- The submitted test must be built with the <b>Angular 12</b> or later.
- The use of `rxjs` is preferable
- The submitied test must be linked to a github or a gitlab repository.
- The code must be clean and readable.
- The design of the page is not important.
 
# API
You will receive a link to the API in the email.  
If you prefer you can run a local server and run your tests against it
 
>You can use the [Postman Collection](https://www.postman.com/winter-moon-273643/workspace/scandinaviatech-public) to understand the API you're working with.
 
### Running Local Server
To run a local server you need to install the dependencies.
```bash
pnpm install
```
then run the local server
```bash
pnpm start
```
 
The server will be running on port 3000