import { Controller, Get } from '@nestjs/common';
import { DataProvider, Vehicle } from './provider.service';

@Controller()
export class AppController {
  constructor(private dataprovider: DataProvider) {}

  @Get('get-vehicles')
  getVehicles(): Promise<Vehicle[]> {
    return this.dataprovider.vehicles();
  }
}
