import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DataProvider } from './provider.service';
import { SocketGateway } from './socket-gateway.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, DataProvider, SocketGateway],
})
export class AppModule {}
