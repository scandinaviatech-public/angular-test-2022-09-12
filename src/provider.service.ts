import { Injectable } from '@nestjs/common';
import { promises } from 'fs';

export interface Vehicle {
  id: string;
  name: string;
}

export interface VehicleWithSteps {
  id: string;
  name: string;
  gps: {
    lng: number;
    lat: number;
  };
  steps: {
    timestamp: Date;
    gps: {
      lng: 36.2798516;
      lat: 33.5235533;
    };
    status: 'moving';
  }[];
}

@Injectable()
export class DataProvider {
  private __json: any[];

  async vehicles(): Promise<Vehicle[]> {
    const json = await this._json();

    return json.map((vehicle) => {
      return {
        id: vehicle.id,
        name: vehicle.name,
      };
    });
  }
  async vehicleWithSteps(id: string): Promise<VehicleWithSteps> {
    const json = await this._json();

    return json
      .map((vehicle) => {
        return {
          id: vehicle.id,
          name: vehicle.name,
          gps: vehicle.gps
            ? {
                lat: vehicle.gps.lat,
                lng: vehicle.gps.lng,
              }
            : null,
          steps: vehicle.steps.map((step) => {
            return {
              timestamp: new Date(step.timestamp),
              gps: step.gps
                ? {
                    lat: step.gps.lat,
                    lng: step.gps.lng,
                  }
                : null,
              status: step.status,
            };
          }),
        };
      })
      .filter((vehicle) => vehicle.id === id)[0];
  }

  private async _json(): Promise<any[]> {
    if (this.__json) {
      return this.__json;
    }

    const jsonFile = await promises.readFile('./vehicles.json', 'utf-8');

    const json: any[] = JSON.parse(jsonFile);

    return json;
  }
}
