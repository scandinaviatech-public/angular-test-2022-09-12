import { Injectable, NotFoundException } from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { delay, mergeMap, of, Subscription } from 'rxjs';
import { Server, Socket } from 'socket.io';
import { DataProvider } from './provider.service';

@Injectable()
@WebSocketGateway({
  cors: {
    origin: '*',
  },
  transports: ['websocket'],
  path: '/socket.io',
})
export class SocketGateway {
  @WebSocketServer()
  server: Server;

  private _simulationMap: Map<string, Subscription | undefined> = new Map();

  constructor(private dataprovider: DataProvider) {}

  @SubscribeMessage('ping')
  async ping(@ConnectedSocket() client: Socket) {
    return 'pong';
  }

  @SubscribeMessage('join')
  async join(@ConnectedSocket() client: Socket, @MessageBody() id: string) {
    client.join(id);

    this.simulate(client, id);

    return true;
  }

  @SubscribeMessage('leave')
  async leave(@ConnectedSocket() client: Socket, @MessageBody() id: string) {
    client.leave(id);

    return true;
  }

  @SubscribeMessage('leave-all')
  async leaveAll(@ConnectedSocket() client: Socket, @MessageBody() id: string) {
    client.rooms.forEach((room) => client.leave(room));

    return true;
  }

  @SubscribeMessage('simulate')
  async simulate(@ConnectedSocket() client: Socket, @MessageBody() id: string) {
    const vehicle = await this.dataprovider.vehicleWithSteps(id);

    if (!vehicle) {
      throw new WsException(new NotFoundException());
    }

    if (this._simulationMap.has(vehicle.id)) {
      const oldSimulate = this._simulationMap.get(vehicle.id);
      oldSimulate.unsubscribe();
    }

    const delayedSteps = this.withDelay(
      vehicle.steps,
      new Date(vehicle.steps[0].timestamp),
    );

    const subscription = of(...delayedSteps)
      .pipe(mergeMap((step) => of(step).pipe(delay(step.delay))))
      .subscribe((payload) => {
        this.server.to(vehicle.id).emit('location', {
          id: vehicle.id,
          ...payload,
        });
      });

    this._simulationMap.set(vehicle.id, subscription);

    return true;
  }

  @SubscribeMessage('stop-simulation')
  async stopSimulation(
    @ConnectedSocket() client: Socket,
    @MessageBody() id: string,
  ) {
    const vehicle = await this.dataprovider.vehicleWithSteps(id);

    if (!vehicle) {
      throw new WsException(new NotFoundException());
    }

    if (this._simulationMap.has(vehicle.id)) {
      const oldSimulate = this._simulationMap.get(vehicle.id);
      oldSimulate.unsubscribe();
    }

    return true;
  }

  private withDelay<T extends { timestamp: Date }>(
    timestamped: T[],
    about: Date,
  ): (T & { delay: number })[] {
    return timestamped.map((step) => {
      return {
        ...step,
        delay: Math.abs(about.getTime() - step.timestamp.getTime()),
      };
    });
  }
}
